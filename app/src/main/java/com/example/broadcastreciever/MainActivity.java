package com.example.broadcastreciever;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.ActivityManager.RunningAppProcessInfo;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

//    Modification in Main to check Git Bash.....!!!

    TextView tvPercent, tvlistApps, tvStorage, tvRam, tvRunningApps, tvBgRunningApps;
    ListView lvAppsList, lvBgApps;
    MyBroadcastReceiver myBroadcastReceiver;
//    ActivityManager activitymanager;
    Context context;
//    List<ActivityManager.RunningAppProcessInfo> RAP ;

    private static final long KILOBYTE = 1024;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        storageProgressBar();

        myBroadcastReceiver = new MyBroadcastReceiver(tvPercent);
        registerReceiver(myBroadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        IntentFilter intentFilter = new IntentFilter("android.intent.action.BATTERY_LOW");
        intentFilter.addAction(Intent.ACTION_BATTERY_LOW);
        MyBroadcastReceiver receiver = new MyBroadcastReceiver(tvPercent);
        this.registerReceiver(receiver, intentFilter);

    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void btnGetInstalledApps(View view) {
        List<ApplicationInfo> listApplicationInfo = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

        String[] stringsList = new String[listApplicationInfo.size()];
//        String[] runningList = new String[bgRunningApps.size()];

        //  for Installed Apps List
        int i = 0;
        for (ApplicationInfo applicationInfo : listApplicationInfo) {
            stringsList[i] = applicationInfo.packageName;
            i++;
        }
        lvAppsList.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, stringsList));
        tvlistApps.setText(listApplicationInfo.size() + " apps are installed");

        //  for Running Apps List

//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            if (UStats.getUsageStatsList(this).isEmpty()) {
//
//                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
//                startActivity(intent);
//
//                return;
//            }
//            ActivityManager activityManager = (ActivityManager) this
//                    .getSystemService(ACTIVITY_SERVICE);
//
//            List<ActivityManager.RunningAppProcessInfo> tasks = activityManager.getRunningAppProcesses();
//
//            List<ActivityManager.RunningTaskInfo> runningTaskInfo = activityManager
//                    .getRunningTasks(Integer.MAX_VALUE);
//            for (int i1 = 0; i1 < tasks.size(); i1++) {
//
//                tvBgRunningApps.setText(tasks.get(0).processName + "\n");
//
//            }
//        }



//        context = getApplicationContext();
//
//        activitymanager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//
//        RAP = activitymanager.getRunningAppProcesses();
//
//        for (ActivityManager.RunningAppProcessInfo processInfo : RAP) {
//
//            tvBgRunningApps.setText(processInfo.processName + "\n");


//        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
//
//        for (int i1 = 0; i1 < tasks.size(); i1++) {
//            Log.d("Running task", "Running task: " + tasks.get(i1).baseActivity.toShortString() + "\t\t ID: " + tasks.get(i1).id);
//            lvBgApps.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, runningList));
//        }

//        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> bgApps = activityManager.getRunningTasks(Integer.MAX_VALUE);
//        String bgApp = activityManager.getRunningTasks(5).get(0).topActivity.getPackageName();
//
//        int i1 = 0;
//        for (ApplicationInfo applicationInfo1 : bgRunningApps) {
//            runningList[i1] = applicationInfo1.processName;
//            i1++;
//        }
//

//        tvRunningBgApps.setText(bgApp + "\n");
//        lvBgApps.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, runningList));
//        tvRunningApps.setText(bgApps.size() + " running Apps");

//        Log.d("Running task", "Running task: " + bgApps.get(i1).baseActivity.toShortString() + "\t\t ID: " + bgApps.get(i1).id);

//        for (int i1 = 0; i1 < bgApps.size(); i1++) {
//            stringsList[i1] = bgApps.get(i1).baseActivity.flattenToShortString();


//                    Collections.singletonList(bgApps.get(i1).baseActivity.toShortString() + "\t\t ID: " + bgApps.get(i1).id)));
//        tvlistApps.setText(listApplicationInfo.size() + " apps are installed");


//        ActivityManager activityManager = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> recentTasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
//        String[] bgAppList = new String[recentTasks.size()];
//
//        for (ActivityManager.RunningTaskInfo activityManager1 : recentTasks) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                bgAppList[i] = String.valueOf(activityManager1.isRunning);
//                i++;
//            }
//        }
//        lvBgApps.setAdapter(new ArrayAdapter<String >(MainActivity.this, android.R.layout.simple_list_item_1, bgAppList));

//            StatFs internalStatFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
//            long internalTotal;
//            long internalFree;
//
//            StatFs externalStatFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
//            long externalTotal;
//            long externalFree;
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
//                internalTotal = (internalStatFs.getBlockCountLong() * internalStatFs.getBlockSizeLong()) / (KILOBYTE * KILOBYTE);
//                internalFree = (internalStatFs.getAvailableBlocksLong() * internalStatFs.getBlockSizeLong()) / (KILOBYTE * KILOBYTE);
//                externalTotal = (externalStatFs.getBlockCountLong() * externalStatFs.getBlockSizeLong()) / (KILOBYTE * KILOBYTE);
//                externalFree = (externalStatFs.getAvailableBlocksLong() * externalStatFs.getBlockSizeLong()) / (KILOBYTE * KILOBYTE);
//            } else {
//                internalTotal = ((long) internalStatFs.getBlockCount() * (long) internalStatFs.getBlockSize()) / (KILOBYTE * KILOBYTE);
//                internalFree = ((long) internalStatFs.getAvailableBlocks() * (long) internalStatFs.getBlockSize()) / (KILOBYTE * KILOBYTE);
//                externalTotal = ((long) externalStatFs.getBlockCount() * (long) externalStatFs.getBlockSize()) / (KILOBYTE * KILOBYTE);
//                externalFree = ((long) externalStatFs.getAvailableBlocks() * (long) externalStatFs.getBlockSize()) / (KILOBYTE * KILOBYTE);
//            }
//            long total = internalTotal + externalTotal;
//            long free = internalFree + externalFree;
//            long used = total - free;
//
//            tvStorage.setText("Storage: " + used + " MB" + " / " + total + " MB");

            RandomAccessFile reader = null;
            String load = null;
            DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
            double totRam = 0;
            String lastValue = "";
            String totalValue = "";
            try {
                reader = new RandomAccessFile("/proc/meminfo", "r");
                load = reader.readLine();

                Pattern p = Pattern.compile("(\\d+)");
                Matcher m = p.matcher(load);
                String value = "";
                while (m.find()) {
                    value = m.group(1);
                }
                reader.close();

                totRam = Double.parseDouble(value);
                // totRam = totRam / 1024;

                double mb = totRam / 1024.0;
                double gb = totRam / 1048576.0;
                double tb = totRam / 1073741824.0;

                if (tb > 1) {
                    lastValue = twoDecimalForm.format(tb).concat(" TB");
                } else if (gb > 1) {
                    lastValue = twoDecimalForm.format(gb).concat(" GB");
                } else if (mb > 1) {
                    lastValue = twoDecimalForm.format(mb).concat(" MB");
                } else {
                    lastValue = twoDecimalForm.format(totRam).concat(" KB");
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }

            ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
            ActivityManager activityManager1 = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            activityManager1.getMemoryInfo(mi);

            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getBlockCount();

            long freeMemory = mi.availMem / 1048576L;
            long totalMemory = mi.totalMem / 1048576L;
            long usedMemory = totalMemory - freeMemory;

            tvRam.setText("RAM: " + lastValue);
        }

    public void btnCleanApps(View view) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myBroadcastReceiver);
    }

    //   Internal Storage display with Progress Bar...
    private void storageProgressBar() {
        final TextView occupiedSpaceText = (TextView)findViewById(R.id.occupiedSpace);
        final TextView freeSpaceText = (TextView)findViewById(R.id.freeSpace);
        final ProgressBar progressIndicator = (ProgressBar)findViewById(R.id.indicator);
        final float totalSpace = DeviceMemory.getInternalStorageSpace();
        final float occupiedSpace = DeviceMemory.getInternalUsedSpace();
        final float freeSpace = DeviceMemory.getInternalFreeSpace();
        final DecimalFormat outputFormat = new DecimalFormat("#.##");

        if (null != occupiedSpaceText) {
            occupiedSpaceText.setText(outputFormat.format(occupiedSpace) + " MB");
        }

        if (null != freeSpaceText) {
            freeSpaceText.setText(outputFormat.format(freeSpace) + " MB / ");
        }

        if (null != progressIndicator) {
            progressIndicator.setMax((int) totalSpace);
            progressIndicator.setProgress((int)occupiedSpace);
        }
    }

    public static class DeviceMemory {

        public static float getInternalStorageSpace() {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
            //StatFs statFs = new StatFs("/data");
            float total = ((float)statFs.getBlockCount() * statFs.getBlockSize()) / 1048576;
            return total;
        }

        public static float getInternalFreeSpace() {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
            //StatFs statFs = new StatFs("/data");
            float free  = ((float)statFs.getAvailableBlocks() * statFs.getBlockSize()) / 1048576;
            return free;
        }

        public static float getInternalUsedSpace() {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
            //StatFs statFs = new StatFs("/data");
            float total = ((float)statFs.getBlockCount() * statFs.getBlockSize()) / 1048576;
            float free  = ((float)statFs.getAvailableBlocks() * statFs.getBlockSize()) / 1048576;
            float busy  = total - free;
            return busy;
        }
    }

    private void init() {
        tvPercent = findViewById(R.id.tvPercentage);
        tvlistApps = findViewById(R.id.tvCountApps);
        lvAppsList = findViewById(R.id.listInstalledApps);
        tvRunningApps = findViewById(R.id.tvRunningApps);
        lvBgApps = findViewById(R.id.lvBgApps);
//        tvStorage = findViewById(R.id.tvStorage);
        tvRam = findViewById(R.id.tvRam);
//        tvBgRunningApps = findViewById(R.id.tvBgRunningApps);
    }
}